import pickle
from pathlib import Path

data = [
    ["MISSION OF THE NAVY",
    "Mission of the Navy, to recruit, train, equip, and organize to deliver combat ready Naval forces to win conflicts and wars while maintaining security and deterrence through sustained forward presence."],
    ["NAVY CORE VALUES",
    "Honor Courage Commitment"],
    ["CHIEF OF NAVAL OPERATION’S CORE ATTRIBUTES",
    "Integrity Accountability Initiative Toughness"],
    ["CODE OF CONDUCT Article 1",
    "I am an American, fighting in the forces which guard my country and our way of life. I am prepared to give my life in their defense."],
    ["CODE OF CONDUCT Article 2",
    "I will never surrender of my own free will. If in command, I will never surrender the members of my command while they still have the means to resist."],
    ["CODE OF CONDUCT Article 3",
    "If I am captured I will continue to resist by all means available. I will make every effort to escape and aid others to escape. I will accept neither parole nor special favors from the enemy."],
    ["CODE OF CONDUCT Article 4",
    "If I become a prisoner of war, I will keep faith with my fellow prisoners. I will give no information or take part in any action which might be harmful to my comrades. If I am senior, I will take command. If not, I will obey the lawful orders of those appointed over me and will back them up in every way."],
    ["CODE OF CONDUCT Article 5",
    "When questioned, should I become a prisoner of war, I am required to give name, rank, service number and date of birth. I will evade answering further questions to the utmost of my ability. I will make no oral or written statements disloyal to my country and its allies or harmful to their cause."],
    ["CODE OF CONDUCT Article 6",
    "I will never forget that I am an American, fighting for freedom, responsible for my actions, and dedicated to the principles which made my country free. I will trust in my God and in the United States of America."],
    ["GENERAL ORDERS OF A SENTRY 1",
    "Take charge of this post and all government property in view."],
    ["GENERAL ORDERS OF A SENTRY 2",
    "Walk my post in a military manner, keeping always on the alert, and observing everything that takes place within sight or hearing."],
    ["GENERAL ORDERS OF A SENTRY 3",
    "Report all violations of orders I am instructed to enforce."],
    ["GENERAL ORDERS OF A SENTRY 4",
    "Repeat all calls from any post more distant from the guard house than my own."],
    ["GENERAL ORDERS OF A SENTRY 5",
    "Quit my post only when properly relieved."],
    ["GENERAL ORDERS OF A SENTRY 6",
    "Receive, obey, and pass on to the sentry who relieves me all orders from the Commanding Officer, Command Duty Officer, Officer of the Deck, and Officers and Petty Officers of the Watch only."],
    ["GENERAL ORDERS OF A SENTRY 7",
    "Talk to no one except in the line of duty."],
    ["GENERAL ORDERS OF A SENTRY 8",
    "Give the alarm in case of fire or disorder."],
    ["GENERAL ORDERS OF A SENTRY 9",
    "Call the Officer of the Deck in any case not covered by instructions."],
    ["GENERAL ORDERS OF A SENTRY 10",
    "Salute all officers and all colors and standards not cased."],
    ["GENERAL ORDERS OF A SENTRY 11",
    "Be especially watchful at night, and during the time for challenging, challenge all persons on or near my post, and to allow no one to pass without proper authority."],
    ["WATCHSTANDING PRINCIPLES 1",
    "RECEIVE SUFFICIENT TRAINING TO PERFORM THE DUTIES OF THE WATCH STATION PRIOR TO ASSIGNMENT."],
    ["WATCHSTANDING PRINCIPLES 2",
    "BE FULLY ATTENTIVE TO THE DUTIES AND RESPONSIBILITIES OF THE ASSIGNED WATCH STATION."],
    ["WATCHSTANDING PRINCIPLES 3",
    "FREQUENTLY REVIEW ALL OF THE EMERGENCY PROCEDURES OF THEIR WATCH STATION IN ORDER TO BE READY TO EXECUTE EMERGENCY PROCEDURES WITHOUT DELAY."],
    ["WATCHSTANDING PRINCIPLES 4",
    "NOT LEAVE THEIR WATCH STATION UNLESS PROPERLY RELIEVED OR SO ORDERED BY THE OFFICER IN CHARGE OF THE WATCH STATION."],
    ["WATCHSTANDING PRINCIPLES 5",
    "KNOW WHOM TO REPORT TO IN THE WATCH ORGANIZATION AND ALL WATCH STANDERS WHO SHALL REPORT TO THEM."],
    ["WATCHSTANDING PRINCIPLES 6",
    "NOT BE ASSIGNED OR ASSUME ANY OTHER DUTIES WHICH MAY DISTRACT THEM FROM THEIR WATCH FUNCTION."],
    ["WATCHSTANDING PRINCIPLES 7",
    "SHALL REPORT ALL VIOLATIONS OF THE UNIT'S REGULATIONS, DIRECTIVES, AND OTHER BREACHES OF GOOD ORDER AND DISCIPLINE; SHALL TRY TO THE UTMOST TO SUPPRESS SUCH VIOLATIONS; SHALL REPORT ANY KNOWN OR PROBABLE VIOLATION OF SAFETY PRECAUTIONS OR SAFETY REGULATIONS; SHALL TRY TO THE UTMOST TO SUPPRESS SUCH VIOLATIONS AND OTHER MALPRACTICE WHICH MAY ENDANGER THE SAFETY OR SECURITY OF A NAVAL UNIT AND ITS PERSONNEL."],
    ["WATCHSTANDING PRINCIPLES 8",
    "IF AUTHORIZED TO CARRY ARMS, SHALL BE INSTRUCTED ON THE FOLLOWING ORDERS TO SENTRIES AND THE CIRCUMSTANCES UNDER WHICH A WEAPON MAY BE FIRED. WATCHES REQUIRING THE CARRYING OF ARMS WILL BE ASSIGNED ONLY TO PERSONS WHO HAVE BEEN TRAINED IN THE FIRING OF THE WEAPON ASSIGNED."],

]
score = [0] * len(data)
while True:
    if Path('score.pkl').is_file():
        with open('score.pkl', 'rb') as file:
        
        # Call load method to deserialze
            score = pickle.load(file)
    index = score.index(min(score))
# for index in range(len(data)):
    # print(score)
    didnotbreak = True
    correct_answers = True
    print(data[index][0])
    print(data[index][1])
    print("Type skp to skip this one.")
    text = ' '
    space = '  '
    for index_b in range(len(data[index][1].replace(",", " ,").replace("."," .").split(" "))):
        text += ((data[index][1].replace(",", " ,").replace("."," .").split(" ")[index_b][0]) + " ")
    for index_c in range(len(data[index][1].replace(",", " ,").replace("."," .").split(" "))):
        if data[index][1].replace(",", " ,").replace("."," .").split(" ")[index_c][0] != "," and data[index][1].replace(",", " ,").replace("."," .").split(" ")[index_c][0] != ".":
            print(text)
            current_word = data[index][1].replace(",", " ,").replace("."," .").split(" ")[index_c].replace(" ","")
            print(space*index_c, current_word[0])
            user_answer = input()
            if user_answer == "skp":
                break
            if current_word == user_answer:
                print("Correct")

            if current_word != user_answer:
                correct_answers = False
                print(current_word)
    if correct_answers:
        score[index] += 1
        with open('score.pkl', 'wb') as file:
        # A new file will be created
            pickle.dump(score, file)